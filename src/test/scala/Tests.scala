import org.scalatest.FunSuite
import pl.prudnicki.cc._

/**
  * Created by patry on 25.10.2016.
  */
class Tests extends FunSuite {
  test("Lack of possibilities set of figures (Two king on 2x2 board)"){
    val board = Board(2)
    board.resetBoard
    val figures: Array[Figure] = Array[Figure](King(), King())
    val boards = ReturnBoards()
    val results = board.spacingFigures(board, figures, boards, 2)
    assert(results.isEmpty == true)
  }

  test("One king on 2x2 board"){
    val board = Board(2)
    board.resetBoard
    val figures: Array[Figure] = Array[Figure](King())
    val boards = ReturnBoards()
    val results = board.spacingFigures(board, figures, boards, 1)
    assert(results.length == 4)
  }

  test("Two kings and one rock on 3x3 board"){
    val board = Board(3)
    board.resetBoard
    val figures: Array[Figure] = Array[Figure](King(), King(), Rock())
    val boards = ReturnBoards()
    val results = board.spacingFigures(board, figures, boards, 3)
    assert(results.length == 4)
  }

  test("Four knights and two rocks on 4x4 board"){
    val board = Board(4)
    board.resetBoard
    val figures: Array[Figure] = Array[Figure](Knight(), Knight(), Knight(), Knight(), Rock(), Rock())
    val boards = ReturnBoards()
    val results = board.spacingFigures(board, figures, boards, 6)
    assert(results.length == 8)
  }
}
