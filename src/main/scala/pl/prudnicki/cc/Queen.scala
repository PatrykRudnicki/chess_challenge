package pl.prudnicki.cc

/**
  * Created by patry on 21.10.2016.
  */
case class Queen() extends Figure {
  private def neighborsOf(board: Board, cell: Cell): Seq[Cell] = {
    for{
      x <- 0 to board.size-1
      y <- 0 to board.size-1
      if (x != cell.x || cell.y != y)
      if ((x == cell.x || y == cell.y) || (Math.abs(cell.x-x) == 1 && Math.abs(cell.y-y) == 1))
    } yield Seq(cellAtPos(x,y, board), cellAtPos(cell.x - x,cell.y - x, board), cellAtPos(cell.x + x,cell.y - x, board), cellAtPos(cell.x - x,cell.y + x, board), cellAtPos(cell.x + x,cell.y + x, board)).distinct.flatten
  }.flatten

  override def validateCell(board: Board, cell: Cell): Boolean = {
    if (cell.status == Free()) {
      if (neighborsOf(board, cell).map(a => a.status.toString).contains("Q") || neighborsOf(board, cell).map(a => a.status.toString).contains("K") || neighborsOf(board, cell).map(a => a.status.toString).contains("N") || neighborsOf(board, cell).map(a => a.status.toString).contains("B") || neighborsOf(board, cell).map(a => a.status.toString).contains("R")) false
      else true
    } else {
      false
    }
  }

  override def changeFields(board: Board, cell: Cell): Unit = {
    if(validateCell(board, cell)){
      for(i <- neighborsOf(board, cell)){
        board.cells(i.x)(i.y) = Cell(i.x, i.y, Nailed())
      }
      board.cells(cell.x)(cell.y) = Cell(cell.x, cell.y, Busy(Queen()))
    }
  }

  override def toString = "Q"
}
