package pl.prudnicki.cc

/**
  * Created by patry on 21.10.2016.
  */
trait Figure {
  def boardContains(x: Int, y: Int, board: Board): Boolean =
    x >= 0 && x < board.size && y >= 0 && y < board.size

  def cellAtPos(x: Int, y: Int, board: Board): Option[Cell] =
    if (boardContains(x,y, board)) Some(board.cells(x)(y))
    else None

  def changeFields(board: Board, cell: Cell): Unit
  def validateCell(board: Board, cell: Cell): Boolean
}