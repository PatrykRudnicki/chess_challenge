package pl.prudnicki.cc

/**
  * Created by patry on 21.10.2016.
  */
object Main {
  def main(args: Array[String]): Unit = {
    val scanner = new java.util.Scanner(System.in)
    println("Enter size of chessboard: ")
    val boardSize = scanner.nextLine()
    val board = Board(boardSize.toInt)
    board.resetBoard
    println("Enter number of Kings: ")
    val numberOfKings = scanner.nextLine().toInt
    println("Enter number of Queens: ")
    val numberOfQueens = scanner.nextLine().toInt
    println("Enter number of Bishops: ")
    val numberOfBishops = scanner.nextLine().toInt
    println("Enter number of Knights: ")
    val numberOfKnights = scanner.nextLine().toInt
    println("Enter number of Rocks: ")
    val numberOfRocks = scanner.nextLine().toInt
    val numberOfFigures = numberOfKings + numberOfQueens + numberOfBishops + numberOfKnights + numberOfRocks
    var figures: Array[Figure] = Array[Figure]()
    for(i <- 1 to numberOfQueens.toInt) figures ++= Array(Queen())
    for(i <- 1 to numberOfRocks.toInt) figures ++= Array(Rock())
    for(i <- 1 to numberOfBishops.toInt) figures ++= Array(Bishop())
    for(i <- 1 to numberOfKnights.toInt) figures ++= Array(Knight())
    for(i <- 1 to numberOfKings.toInt) figures ++= Array(King())
    val boards = ReturnBoards()
    val results = board.spacingFigures(board, figures, boards, numberOfFigures)
    if(results.isEmpty){
      println("Lack of possibilities set of figures")
    } else {
      for(i <- 0 to results.length-1){
        print(results(i).map(_.mkString).mkString("\n"))
        println()
        println()
      }
    }
  }
}
