package pl.prudnicki.cc

/**
  * Created by patry on 21.10.2016.
  */
case class Cell(x: Int, y: Int, status: Status) {
  override def toString = status.toString
}
