package pl.prudnicki.cc

/**
  * Created by patry on 21.10.2016.
  */
case class Board(size: Int) {
  var cells: Array[Array[Cell]] = Array.ofDim[Cell](size, size)

  def printBoard: Unit = print(cells.map(_.mkString).mkString("\n"))

  def resetBoard: Unit = {
    for {
      x <- 0 to size - 1
      y <- 0 to size - 1
    } yield cells(x)(y) = Cell(x, y, Free())
  }

  private def checkAllFiguresOnBoard(board: Array[Array[Cell]], numberOfFigures: Int): Boolean = {
    var actualNumberOfFigures = 0
    for{
      x <- 0 to board.length-1
      y <- 0 to board.length-1
      if(board(x)(y).status != Free() && board(x)(y).status !=  Nailed())
    } yield actualNumberOfFigures += 1
    if(actualNumberOfFigures == numberOfFigures) true
    else false
  }

  private def copyBoard(board: Board): Array[Array[Cell]] = {
    val board_tmp = Array.ofDim[Cell](board.size, board.size)
    for{
      i <- 0 to board.size-1
      j <- 0 to board.size-1
    } board_tmp(i)(j) = board.cells(i)(j)
    board_tmp
  }

  private def checkDifferentBoard(cells: Array[Array[Cell]], boards: Seq[Array[Array[Cell]]]): Boolean = {
    val checkTheSameBoard = boards.map{ board =>
      if(board.deep == cells.deep) true else false}
    !checkTheSameBoard.contains(true)
  }

  def spacingFigures(board: Board, figures: Array[Figure], returnBoards: ReturnBoards, numberOfFigures: Int): Seq[Array[Array[Cell]]] = {
    for {
      figure <- figures
      i <- 0 to board.size - 1
      j <- 0 to board.size - 1
      if(board.cells(i)(j).status == Free())
      if(figures(0).validateCell(board, Cell(i,j,Free())))
    } yield {
      var board_tmp = Board(board.size)
      board_tmp.cells = copyBoard(board)
      figures(0).changeFields(board_tmp, Cell(i,j,Free()))
      if(figures.length == 1){
        val result: Array[Array[Cell]] = copyBoard(board_tmp)
        if(checkDifferentBoard(result, returnBoards.boards) && checkAllFiguresOnBoard(result, numberOfFigures)){
          returnBoards.boards = returnBoards.boards ++ Seq(result)
        }
      } else {
        spacingFigures(board_tmp, figures.tail, returnBoards, numberOfFigures)
      }
    }
    returnBoards.boards
  }
}
