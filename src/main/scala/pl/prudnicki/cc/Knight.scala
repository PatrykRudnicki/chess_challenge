package pl.prudnicki.cc

/**
  * Created by patry on 21.10.2016.
  */
case class Knight() extends Figure {
  private def neighborsOf(board: Board, cell: Cell): Seq[Cell] = {
    for{
      x <- (cell.x - 2) to (cell.x + 2)
      y <- (cell.y - 2) to (cell.y + 2)
      if (x != cell.x || cell.y != y)
      if (Math.abs(x - cell.x) == 2 && Math.abs(y - cell.y) == 1 || Math.abs(x - cell.x) == 1 && Math.abs(y - cell.y) == 2)
    } yield cellAtPos(x,y, board)
  }.flatten

  override def validateCell(board: Board, cell: Cell): Boolean = {
    if (cell.status == Free()) {
      if (neighborsOf(board, cell).map(a => a.status.toString).contains(List("X")) || neighborsOf(board, cell).map(a => a.status.toString).contains("N")) false
      else true
    } else {
      false
    }
  }

  override def changeFields(board: Board, cell: Cell): Unit = {
    if (validateCell(board, cell)) {
      board.cells(cell.x)(cell.y) = Cell(cell.x,cell.y,Busy(Knight()))
      for(i <- neighborsOf(board, cell)){
        board.cells(i.x)(i.y) = Cell(i.x,i.y,Nailed())
      }
    }
  }

  override def toString = "N"
}
