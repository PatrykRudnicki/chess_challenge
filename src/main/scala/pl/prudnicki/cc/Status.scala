package pl.prudnicki.cc

/**
  * Created by patry on 21.10.2016.
  */
trait Status

case class Free() extends Status {
  override def toString = "."
}
case class Busy(figure: Figure) extends Status {
  override def toString = figure.toString
}
case class Nailed() extends Status {
  override def toString = "X"
}

