package pl.prudnicki.cc

/**
  * Created by patry on 21.10.2016.
  */
case class King() extends Figure {

  private def neighborsOf(board: Board, cell: Cell): Seq[Cell] = {
    for{
      x <- (cell.x - 1) to (cell.x + 1)
      y <- (cell.y - 1) to (cell.y + 1)
      if (x != cell.x || cell.y != y)
    } yield cellAtPos(x,y, board)
  }.flatten

  override def validateCell(board: Board, cell: Cell): Boolean = {
    if (cell.status == Free()) {
      if (neighborsOf(board, cell).map(a => a.status.toString).contains("Q") || neighborsOf(board, cell).map(a => a.status.toString).contains("K") || neighborsOf(board, cell).map(a => a.status.toString).contains("N") || neighborsOf(board, cell).map(a => a.status.toString).contains("B") || neighborsOf(board, cell).map(a => a.status.toString).contains("R")) false
      else true
    } else {
      false
    }
  }

  override def changeFields(board: Board, cell: Cell): Unit = {
    if (validateCell(board, cell)) {
      board.cells(cell.x)(cell.y) = Cell(cell.x,cell.y,Busy(King()))
      for(i <- neighborsOf(board, cell)){
        board.cells(i.x)(i.y) = Cell(i.x,i.y,Nailed())
      }
    }
  }

  override def toString = "K"
}
